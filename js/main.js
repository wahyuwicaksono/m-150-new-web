$(document).ready(function() {
    // Back to Top click scroll to top
    $("#backToTop").click(function () {
        $("html").animate({ scrollTop: 0 }, "slow");
    });
});